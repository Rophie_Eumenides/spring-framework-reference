[[spring-mvc-test-framework]]
==== Spring MVC Test Framework

.Standalone project
****
Before inclusion in Spring Framework 3.2, the Spring MVC Test framework had already
existed as a separate project on GitHub where it grew and evolved through actual use,
feedback, and the contribution of many.

The standalone https://github.com/spring-projects/spring-test-mvc[spring-test-mvc project]
is still available on GitHub and can be used in conjunction with Spring Framework 3.1.x.
Applications upgrading to 3.2 or later should replace the `spring-test-mvc` dependency with a
dependency on `spring-test`.

The `spring-test` module uses a different package `org.springframework.test.web` but
otherwise is nearly identical with two exceptions. One is support for features new in
3.2 (e.g. asynchronous web requests). The other relates to the options for creating a
`MockMvc` instance. In Spring Framework 3.2 and later, this can only be done through the
TestContext framework, which provides caching benefits for the loaded configuration.
****

The __Spring MVC Test framework__ provides first class JUnit support for testing client
and server-side Spring MVC code through a fluent API. Typically it loads the actual
Spring configuration through the __TestContext framework__ and always uses the
`DispatcherServlet` to process requests thus approximating full integration tests
without requiring a running Servlet container.

Client-side tests are `RestTemplate`-based and allow tests for code that relies on the
`RestTemplate` without requiring a running server to respond to the requests.


[[spring-mvc-test-server]]
===== Server-Side Tests
Before Spring Framework 3.2, the most likely way to test a Spring MVC controller was to
write a unit test that instantiates the controller, injects it with mock or stub
dependencies, and then calls its methods directly, using a `MockHttpServletRequest` and
`MockHttpServletResponse` where necessary.

Although this is pretty easy to do, controllers have many annotations, and much remains
untested. Request mappings, data binding, type conversion, and validation are just a few
examples of what isn't tested. Furthermore, there are other types of annotated methods
such as `@InitBinder`, `@ModelAttribute`, and `@ExceptionHandler` that get invoked as
part of request processing.

The idea behind Spring MVC Test is to be able to re-write those controller tests by
performing actual requests and generating responses, as they would be at runtime, along
the way invoking controllers through the Spring MVC `DispatcherServlet`. Controllers can
still be injected with mock dependencies, so tests can remain focused on the web layer.

Spring MVC Test builds on the familiar "mock" implementations of the Servlet API
available in the `spring-test` module. This allows performing requests and generating
responses without the need for running in a Servlet container. For the most part
everything should work as it does at runtime with the exception of JSP rendering, which
is not available outside a Servlet container. Furthermore, if you are familiar with how
the `MockHttpServletResponse` works, you'll know that forwards and redirects are not
actually executed. Instead "forwarded" and "redirected" URLs are saved and can be
asserted in tests. This means if you are using JSPs, you can verify the JSP page to
which the request was forwarded.

All other means of rendering including `@ResponseBody` methods and `View` types (besides
JSPs) such as Freemarker, Velocity, Thymeleaf, and others for rendering HTML, JSON, XML,
and so on should work as expected, and the response will contain the generated content.

Below is an example of a test requesting account information in JSON format:

[source,java,indent=0]
----
	import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
	import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

	@RunWith(SpringJUnit4ClassRunner.class)
	@WebAppConfiguration
	@ContextConfiguration("test-servlet-context.xml")
	public class ExampleTests {

		@Autowired
		private WebApplicationContext wac;

		private MockMvc mockMvc;

		@Before
		public void setup() {
			this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
		}

		@Test
		public void getAccount() throws Exception {
			this.mockMvc.perform(get("/accounts/1").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.name").value("Lee"));
		}

	}
----

The test relies on the `WebApplicationContext` support of the __TestContext framework__.
It loads Spring configuration from an XML configuration file located in the same package
as the test class (also supports JavaConfig) and injects the created
`WebApplicationContext` into the test so a `MockMvc` instance can be created with it.

The `MockMvc` is then used to perform a request to `"/accounts/1"` and verify the
resulting response status is 200, the response content type is `"application/json"`, and
response content has a JSON property called "name" with the value "Lee". JSON content is
inspected with the help of Jayway's https://github.com/jayway/JsonPath[JsonPath
project]. There are lots of other options for verifying the result of the performed
request and those will be discussed later.

